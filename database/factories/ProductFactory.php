<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name_de'           => $faker->name,
        'name_fr'           => $faker->name,
        'title_de'          => $faker->text(50),
        'title_fr'          => $faker->text(50),
        'description_de'    => $faker->text,
        'description_fr'    => $faker->text,
    ];
});
