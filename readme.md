#Configuration

- After cloning this repo, configure the Laravel .env file with your database details

#Installtion - Run the following commands:

- composer install
- php artisan migrate
- php artisan db:Seed

TO run as a local server execute the following command:

- php artisan serv

#Post Installation

Access the backend via the route **/login** using the following admin user:
**username:** admin@gsdh.com
**password:** temp@12
