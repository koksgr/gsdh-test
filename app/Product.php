<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $fillable = [
        'name_de',
        'name_fr',
        'title_de',
        'title_fr',
        'description_de',
        'description_fr',
        'image'
    ];
}
