<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;

class ProductsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
        if(isset($_GET['locale'])) {
            \App::setLocale($_GET['locale']);
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        $locale = \App::getLocale();
        return view('products', compact('products', 'locale'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('add-product');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        foreach($request->all() as $key => $value) {
            if ($key === '_token') continue;
            if ($request->hasFile($key)) {
                $file = $request->file($key);
                $filename = $file->getClientOriginalName();
                $file->move(public_path().'/img/', $filename);
                $data['image'] = $filename;
                break;
            }
        }
        $product = Product::create($data);
        return redirect('/dash');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
        return view('edit-product', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data = $request->all();
        foreach($request->all() as $key => $value) {
            if ($key === '_token') continue;
            if ($request->hasFile($key)) {
                $file = $request->file($key);
                $filename = $file->getClientOriginalName();
                $file->move(public_path().'/img/', $filename);
                $data['image'] = $filename;
                break;
            }
        }
        Product::find($request->id)->update($data);
        return redirect('/dash');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        unlink(public_path().'/img/'.$product->image);
        $product->delete();
        return redirect('/dash');
    }
}
