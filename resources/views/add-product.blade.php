@extends('layouts.admin')

@section('content')
<div class="container">
    <h1>Add Product</h1>
    <div class="card">
        <div class="card-body">
            <form class="needs-validation" method="POST"  action="/products/add" accept-charset="UTF-8" enctype="multipart/form-data">
                @csrf
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name_de">Product Name (DE)</label>
                        <input type="text" class="form-control" id="name_de" name="name_de" placeholder="Your Product Name in Dutch" required>
                        <div class="invalid-tooltip">
                            Please provide a valid city.
                        </div>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="name_fr">Product Name (FR)</label>
                        <input type="text" class="form-control" id="name_fr" name="name_fr" placeholder="Your Product Name in French" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name_de">Product Title (DE)</label>
                        <input type="text" class="form-control" id="title_de" name="title_de" placeholder="Your Product Title in Dutch" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="title_fr">Product Title (FR)</label>
                        <input type="text" class="form-control" id="title_fr" name="title_fr" placeholder="Your Product Title in French" required>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="name_de">Product Description (DE)</label>
                        <textarea rows="10" class="form-control" id="description_de" name="description_de" placeholder="Your Product Description in Dutch" required></textarea>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="description_fr">Product Description (FR)</label>
                        <textarea rows="10" class="form-control" id="description_fr" name="description_fr" placeholder="Your Description Title in French" required></textarea>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                        <label for="description_fr">Select Product Image (.png or .jpg format only)</label>
                        <input type="file" name="image" id="image" required/>
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-6">
                    </div>
                    <div class="form-group col-md-6">
                            <button class="btn btn-primary float-right" type="submit">
                                    <i class="fa fa-plus" aria-hidden="true"></i> Add Product
                            </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection

@section('script')
    <script src="{{ URL::asset('js/file-upload-lib.js') }}"></script>
    <script src="{{ URL::asset('js/admin.js') }}"></script>
@endsection
