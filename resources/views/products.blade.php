@extends('layouts.public')

@section('content')

<h1 class="content-title">PRODUKTE</h1>

<nav aria-label="breadcrumb">
    <ol class="breadcrumb">
        <li class="breadcrumb-item">Home</li>
        <li class="breadcrumb-item">Wärme - Kälte</li>
        <li class="breadcrumb-item active" aria-current="page">Automatikfilter</li>
    </ol>
</nav>

<?php $counter = 0 ?>

@foreach ($products as $idx => $product)
@if ($counter > 0)
        <div class="row product" style="background:#f5f5f5">
        <?php $counter = -1; ?>
@else
        <div class="row product">
@endif
        <div class="col-md-3">
            <img class="img-fluid" src="/img/{{ $product->image }}" style="margin-bottom:10px"/>
        </div>
        <div class="col-md-9">
            <div class="holder" style="padding-left:15px">
                @if ($locale === 'de')
                    <h2>{{ $product->name_de }}</h2>
                    @if ( !isset(explode('-', $product->title_de)[1])  )
                        <h4><b>{{ $product->title_de }}</b></h4>
                    @else
                        <h4><b>{{ explode('-', $product->title_de)[0] }}</b> - {{ explode('-', $product->title_de)[1] }}</h4>
                    @endif
                    <p>{{ $product->description_de }}</p>
                @else
                <h2>{{ $product->name_fr }}</h2>
                @if ( !isset(explode('-', $product->title_fr)[1])  )
                        <h4><b>{{ $product->title_fr }}</b></h4>
                    @else
                        <h4><b>{{ explode('-', $product->title_fr)[0] }}</b> - {{ explode('-', $product->title_fr)[1] }}</h4>
                    @endif
                <p>{{ $product->description_fr }}</p>
                @endif
                <button class="btn product btn-primary float-right" type="submit">
                        Mehr Erfanren&nbsp;&nbsp;&nbsp;&nbsp;>
                </button>
            </div>
        </div>
    </div>
<?php ++$counter ?>
@endforeach

@endsection
