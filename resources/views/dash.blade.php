@extends('layouts.admin')

@section('content')
<div class="container">
    <h1>Products</h1>
    <div class="card">
        <div class="card-body">
            @if (count($products) > 0)
                <div class="row">
                    @foreach ($products as $product)
                        <div class="col-md-4">
                            <div class="card text-center" style="margin-bottom:20px">
                                <img class="card-img-top" src="/img/{{ $product->image }}" style="height: 13rem;">
                                <div class="card-body">
                                    <h2 class="card-title">{{ ($locale === 'fr') ? $product->name_fr : $product->name_de }}</h2>
                                    <a href="/products/{{ $product->id }}" class="btn btn-primary">Edit Product</a>
                                    <a href="/products/delete/{{ $product->id }}" class="btn btn-danger">Delete Product</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            @else
                No Existing Products. <a href="/products/add">Create Product?</a>
            @endif
          </div>
    </div>
</div>
@endsection
