<footer>
    <div class="container">
        <div class="row">
                <div class="col-sm-12 col-md-7">
                    &copy; Elg Crustag 2014 <br><br>
                    Impressum
                </div>
                <div class="col-sm-12 col-md-5">

                    <div class="row">
                            <div class="col-md-6">
                                Buro Baar ZG <br><br>
                                <div class="row">
                                    <div class="col-xs-4 col-sm-4 col-md-2">
                                            <img class="img-fluid" src="/img/mobile.gif" />
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-10">
                                            ( Tel ) +41 22 735 95 00
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                            <img class="img-fluid" src="/img/fax.gif" />
                                    </div>
                                    <div class="col-md-10">
                                            ( Fax ) +41 22 735 96 00
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                            <img class="img-fluid" src="/img/location.gif" />
                                    </div>
                                    <div class="col-md-10">
                                        Route de Frontenex 120,<br>
                                        1208 Genève<br><br>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                Bureau Genève <br><br>
                                <div class="row">
                                        <div class="col-md-2">
                                                <img class="img-fluid" src="/img/mobile.gif" />
                                        </div>
                                        <div class="col-md-10">
                                                (Tel) +41 22 735 95 00
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-2">
                                                <img class="img-fluid" src="/img/fax.gif" />
                                        </div>
                                        <div class="col-md-10">
                                                (Fax) +41 22 735 96 00
                                        </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-2">
                                            <img class="img-fluid" src="/img/location.gif" />
                                    </div>
                                    <div class="col-md-10">
                                        Route de Frontenex 120,<br>
                                        1208 Genève
                                    </div>
                                </div>
                            </div>
                    </div>

                </div>
        </div>
    </div>
</footer>
