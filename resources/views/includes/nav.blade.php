<nav class="navbar navbar-expand-md navbar-light navbar-laravel">
    <div class="container">
        @auth
            <a class="navbar-brand" href="{{ url('/dash') }}">
                <img src="/img/logo.gif" />
            </a>
        @endauth
        @guest
            <a class="navbar-brand" href="{{ url('/products') }}?locale={{ App::getLocale() }}">
                <img src="/img/logo.gif" />
            </a>
        @endguest
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @auth
                    <li class="{{ (\Request::route()->getName() === 'add-product') ? 'active' : '' }} nav-item">
                        <a class="nav-link" href="/products/add?locale={{ App::getLocale() }}">Create Product</a>
                    </li>
                @endauth
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                @guest
                    <li class="nav-item" id="main">
                    <a class="nav-link" href="">HOME</a>
                    </li>
                    <li class="nav-item" id="main">
                    <a class="nav-link" href="">Über uns</a>
                    </li>
                    <li class="nav-item {{ (\Request::route()->getName() === 'products') ? 'active' : '' }}" id="main">
                    <a class="nav-link" href="/products?locale={{ App::getLocale() }}">Produkte</a>
                    </li>
                    <li class="nav-item" id="main">
                        <a class="nav-link" href="">Referenzen</a>
                    </li>
                    <li class="nav-item" id="main" style="margin-right:30px">
                        <a class="nav-link" href="">Kontakt</a>
                    </li>
                    <li class="{{ (\App::getLocale() === 'de') ? 'active' : '' }} nav-item" id="lang">
                            <a class="nav-link" href="{{ url()->current().'?locale=de' }}">DE</a>
                    </li>
                    <li class="nav-item splitter" id="lang">
                        <div class="nav-link">/</div></li>
                    <li class="{{ (\App::getLocale() === 'fr') ? 'active' : '' }} nav-item" id="lang">
                        <a class="nav-link" href="{{ url()->current().'?locale=fr' }}">FR</a>
                    </li>
                @endguest
                <!-- Authentication Links -->
                @auth
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endauth
            </ul>
        </div>
    </div>
</nav>
