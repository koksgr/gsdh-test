<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Product;

class AppDbTest extends TestCase
{

    use RefreshDatabase;

    protected $table;

    public function setUp() {
        parent::setUp();
        $this->table = 'products';
    }

    /**
     * Test if model can be inserted
     * into db table.
     *
     * @return void
     */
    public function testProductCreate() {
        $product = factory(Product::class)->create();
        $this->assertDatabaseHas($this->table, $product->toArray());
    }

    /**
     * Test if Product update works
     *
     * @return void
     */
    public function testProductUpdate() {
        $product = Product::all()->first();
        $data = $product->toArray();
        $product->name_de = "Gershon Koks";
        $product->save();
        $this->assertDatabaseMissing($this->table, $data);
    }

    /**
     * Test if Produce delete works
     *
     * @return void
     */
    public function testProductDelete() {
        $product = Product::all()->first();
        $product->delete();
        $this->assertDatabaseMissing($this->table, $product->toArray());
    }

}
